import { Component, OnInit, Input } from '@angular/core';
import { RestService } from './rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'contract';

  contracts:any = [];

  @Input() data = { currencyCode:'PLN', daysRate: 1000.0 };

  ngOnInit() {
    this.contracts = []
  }

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  contract(){

    this.rest.showContracts(this.data).subscribe((result : {}) => {
      console.log(result);
      this.contracts = result;
    })
  }
}
