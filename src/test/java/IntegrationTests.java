import com.sonalake.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class IntegrationTests {
    @Autowired
    private MockMvc mvc;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(IntegrationTests.class);

    @Test
    public void countContractWithoutInputData() throws Exception {
        mvc.perform(post("/contract")
                .contentType("application/json;charset=UTF-8")
                .accept("application/json;charset=UTF-8"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void countContractWithWrongInputData() throws Exception {
        mvc.perform(post("/contract")
                .contentType("application/json;charset=UTF-8")
                .content("{\n" +
                        "\t\"daysRate\" : 100.23,\n" +
                        "\t\"currencyCode\" : \"USD\"\n" +
                        "}")
                .accept("application/json;charset=UTF-8"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void countContractWithOkInputData() throws Exception {
        mvc.perform(post("/contract")
                .contentType("application/json;charset=UTF-8")
                .content("{\n" +
                        "\t\"daysRate\" : 100.23,\n" +
                        "\t\"currencyCode\" : \"PLN\"\n" +
                        "}")
                .accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk());
    }
}
