package com.sonalake.request;

import com.sonalake.config.Currency;
import com.sonalake.config.EnumValidator;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class ContractRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "Days Rate should not be null!")
    @Min(value = 1, message = "Days Rate Amount should not be less than 1")
    private Double daysRate;

    @NotNull(message = "Currency Code should not be null!")
    @EnumValidator(enumClass = Currency.class, message = "Wrong currency code")
    private String currencyCode;


    public Double getDaysRate() {
        return daysRate;
    }

    public void setDaysRate(Double daysRate) {
        this.daysRate = daysRate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
