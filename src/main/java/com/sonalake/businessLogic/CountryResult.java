package com.sonalake.businessLogic;


import com.sonalake.country.Country;

public class CountryResult {
    private Country country;
    private Double amount;
    private Double amountInLocalCurrency;

    public CountryResult(Country country, Double amount, Double amountInLocalCurrency) {
        this.country = country;
        this.amount = amount;
        this.amountInLocalCurrency = amountInLocalCurrency;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmountInLocalCurrency() {
        return amountInLocalCurrency;
    }

    public void setAmountInLocalCurrency(Double amountInLocalCurrency) {
        this.amountInLocalCurrency = amountInLocalCurrency;
    }
}
