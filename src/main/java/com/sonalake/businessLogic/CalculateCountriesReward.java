package com.sonalake.businessLogic;


import com.sonalake.config.MonthlyCostsConfig;
import com.sonalake.config.TaxRateConfig;
import com.sonalake.country.CostCalculator;
import com.sonalake.country.Country;
import com.sonalake.country.StandardCostCalculator;
import com.sonalake.exchange.Exchange;
import com.sonalake.exchange.NbpExchange;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

public class CalculateCountriesReward {

    public static List<CountryResult> calculate(Currency input, Double rewardDaily) throws Exception {
        Double monthlyReward = 22 * rewardDaily;
        Exchange nbpExchange = new NbpExchange();
        Double amountInEur = nbpExchange.exchange(input, Currency.getInstance("EUR"), monthlyReward);
        Double amountInPln = nbpExchange.exchange(input, Currency.getInstance("PLN"), monthlyReward);
        Double amountInGbp = nbpExchange.exchange(input, Currency.getInstance("GBP"), monthlyReward);

        Country germanCountry = new Country("Germany", MonthlyCostsConfig.GERMAN.getMonthlyCost(), TaxRateConfig.GERMAN.getTaxRate(), Currency.getInstance("EUR"));
        Country polandCountry = new Country("Poland", MonthlyCostsConfig.POLAND.getMonthlyCost(), TaxRateConfig.POLAND.getTaxRate(), Currency.getInstance("PLN"));
        Country ukCountry = new Country("UK", MonthlyCostsConfig.UK.getMonthlyCost(), TaxRateConfig.UK.getTaxRate(), Currency.getInstance("GBP"));

        CostCalculator costCalculator = new StandardCostCalculator();
        Double germanMonthlyReward = costCalculator.monthlyRewardCalculator(germanCountry, amountInEur);
        Double polandMonthlyReward = costCalculator.monthlyRewardCalculator(polandCountry, amountInPln);
        Double ukMonthlyReward = costCalculator.monthlyRewardCalculator(ukCountry, amountInGbp);

        Double germanMonthlyRewardInPln =  nbpExchange.exchange(Currency.getInstance("EUR"), Currency.getInstance("PLN"), germanMonthlyReward);
        Double ukMonthlyRewardInPln =  nbpExchange.exchange(Currency.getInstance("GBP"), Currency.getInstance("PLN"), ukMonthlyReward);

        List<CountryResult> countryResults = new ArrayList<>();
        countryResults.add(new CountryResult(germanCountry, germanMonthlyReward, germanMonthlyRewardInPln));
        countryResults.add(new CountryResult(polandCountry, polandMonthlyReward, polandMonthlyReward));
        countryResults.add(new CountryResult(ukCountry, ukMonthlyReward, ukMonthlyRewardInPln));
        return countryResults;
    }
}
