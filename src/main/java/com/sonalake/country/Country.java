package com.sonalake.country;

import java.util.Currency;

public class Country {

    private String name;

    private Double monthlyCost;

    private Double taxRate;

    private Currency currency;

    public Country(String name, Double monthlyCost, Double taxRate, Currency currency) {
        this.name = name;
        this.monthlyCost = monthlyCost;
        this.taxRate = taxRate;
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMonthlyCost() {
        return monthlyCost;
    }

    public void setMonthlyCost(Double monthlyCost) {
        this.monthlyCost = monthlyCost;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
