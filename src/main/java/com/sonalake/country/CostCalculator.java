package com.sonalake.country;

public interface CostCalculator {
    Double monthlyRewardCalculator(Country country, Double value);
}
