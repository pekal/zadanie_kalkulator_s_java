package com.sonalake.country;


public class StandardCostCalculator implements CostCalculator {
    @Override
    public Double monthlyRewardCalculator(Country country, Double value) {
        return (1 - (country.getTaxRate() / 100)) * value - country.getMonthlyCost();
    }
}
