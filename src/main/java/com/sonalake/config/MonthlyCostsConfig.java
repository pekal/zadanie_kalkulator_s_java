package com.sonalake.config;

public enum MonthlyCostsConfig {
    GERMAN(800.0),
    POLAND(1200.0),
    UK(600.0);

    private final Double monthlyCost;

    MonthlyCostsConfig(double v) {
        this.monthlyCost = v;
    }

    public Double getMonthlyCost() {
        return monthlyCost;
    }
}