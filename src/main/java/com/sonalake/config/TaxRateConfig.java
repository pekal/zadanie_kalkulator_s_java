package com.sonalake.config;

public enum TaxRateConfig {
    GERMAN(20.0),
    POLAND(19.0),
    UK(25.0);

    private final Double taxRate;

    TaxRateConfig(double v) {
        this.taxRate = v;
    }

    public Double getTaxRate() {
        return taxRate;
    }
}