package com.sonalake.api;

import com.sonalake.businessLogic.CalculateCountriesReward;
import com.sonalake.businessLogic.CountryResult;
import com.sonalake.request.ContractRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Currency;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ContractController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractController.class);

    @PostMapping("/contract")
    public List<CountryResult> create(@Valid @RequestBody ContractRequest contractRequest) throws Exception {
        return CalculateCountriesReward.calculate(Currency.getInstance(contractRequest.getCurrencyCode()), contractRequest.getDaysRate());
    }

}
