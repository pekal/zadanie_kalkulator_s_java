package com.sonalake.exchange;

import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Currency;

public class NbpExchange implements Exchange{
    private static final Logger LOGGER = LoggerFactory.getLogger(NbpExchange.class);

    @Override
    public Double exchange(Currency input, Currency output, Double value) throws Exception {
        if (!input.getCurrencyCode().equalsIgnoreCase(com.sonalake.config.Currency.PLN.toString()) && !output.getCurrencyCode().equalsIgnoreCase(com.sonalake.config.Currency.PLN.toString())) {
            throw new Exception("to use nbp api one currency should be PLN");
        }
        if(input.equals(output)) {
            return value;
        }
        Client client = ClientBuilder.newClient();
        String currencyCode = input.getCurrencyCode();
        boolean outputPln = true;
        if (input.getCurrencyCode().equalsIgnoreCase(com.sonalake.config.Currency.PLN.toString())) {
            currencyCode = output.getCurrencyCode();
            outputPln = false;
        }

        WebTarget webTarget = client.target("http://api.nbp.pl/api/exchangerates/rates/C").path(currencyCode).queryParam("format", "json");

        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        if (response.getStatus() != 200) {
            throw new Exception("nbp api return " + response.getStatus() + " with info " + response.readEntity(String.class));
        }
        NbpEntity nbpEntity = response.readEntity(NbpEntity.class);
        LOGGER.info(nbpEntity.toString());

        if (outputPln) {
            return value * nbpEntity.getRates()[0].getAsk();
        }
        return value / nbpEntity.getRates()[0].getBid();
    }
}
