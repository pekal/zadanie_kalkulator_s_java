package com.sonalake.exchange;

import java.util.Arrays;

public class NbpEntity {
    String table;
    String currency;
    String code;
    RateEntity rates[];

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RateEntity[] getRates() {
        return rates;
    }

    public void setRates(RateEntity[] rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "NbpEntity{" +
                "table='" + table + '\'' +
                ", currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", rates=" + Arrays.toString(rates) +
                '}';
    }
}


