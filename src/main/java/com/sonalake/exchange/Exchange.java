package com.sonalake.exchange;

import java.util.Currency;

public interface Exchange {
    Double exchange(Currency input, Currency output, Double value) throws Exception;
}
